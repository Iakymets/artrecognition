package com.example.bohdan.artrecognition;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {
    public String prediction;
    public ArtWork artwork;
    Response(@JsonProperty("prediction") String prediction, @JsonProperty("artwork") ArtWork artwork) {
        this.prediction = prediction;
        this.artwork = artwork;
    }
    @JsonIgnoreProperties(ignoreUnknown = true)
    static public class ArtWork {
        public String id;
        public String title;
        public String description;
        public String author;
        public String genre;
        public int year;
        ArtWork(
                @JsonProperty("id") String i,
                @JsonProperty("title") String t,
                @JsonProperty("description") String d,
                @JsonProperty("author") String a,
                @JsonProperty("genre") String g,
                @JsonProperty("year") int y
        ) {
            this.id = i;
            this.title = t;
            this.description = d;
            this.author = a;
            this.genre = g;
            this.year = y;
        }
    }
}

