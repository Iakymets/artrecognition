package com.example.bohdan.artrecognition;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.vision.barcode.Barcode;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import static org.opencv.core.CvType.CV_32F;
import static org.opencv.core.CvType.CV_8U;

public class MainActivity extends Activity implements PortraitCameraBridgeViewBase.CvCameraViewListener2, LocationListener {

    private static final String TAG = "OCVSample::Activity";
    private static final int BARCODE_READER_ACTIVITY_REQUEST = 1;
    private boolean enableRequest = true;
    private PortraitCameraBridgeViewBase mOpenCvCameraView;
    private String locationId = "";
    private BottomSheetBehavior bottomSheetBehavior;
    protected LocationManager locationManager;
    protected LocationListener locationListener;
    private LinearLayout llBottomSheet;
    TextView tvName;
    TextView tvDescription;
    TextView tvAuthor;
    TextView tvYear;
    TextView tvGenre;
    LinearLayout drawPanel;
    FeatureDetector detector;
    DescriptorExtractor descriptor;
    DescriptorMatcher matcher;
    Mat descriptors1;
    Mat img1 = null;
    ImageRecognizer task;

    float scale;

    static {
        if (!OpenCVLoader.initDebug())
            Log.d("ERROR", "Unable to load OpenCV");
        else
            Log.d("SUCCESS", "OpenCV loaded");
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.enableView();
                    detector = FeatureDetector.create(FeatureDetector.ORB);
                    descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB);
                    matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };


    public MainActivity() {

        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        mOpenCvCameraView = (PortraitCameraBridgeViewBase) findViewById(R.id.tutorial1_activity_java_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        scale = getResources().getDisplayMetrics().density;

        tvName = (TextView) findViewById(R.id.text1);
        Log.d(TAG, tvName.toString());
        tvDescription = (TextView) findViewById(R.id.description);
        tvAuthor = (TextView) findViewById(R.id.author);
        tvGenre = (TextView) findViewById(R.id.genre);
        tvYear = (TextView) findViewById(R.id.year);
        task = new ImageRecognizer();

        llBottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setPeekHeight(340);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED|| newState == BottomSheetBehavior.STATE_DRAGGING) {
                    enableRequest = false;
                    Log.i(TAG, "onPanelStateChanged " + newState);
                } else {
                    enableRequest = true;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Intent launchIntent = BarcodeReaderActivity.getLaunchIntent(this, true, false);
            startActivityForResult(launchIntent, BARCODE_READER_ACTIVITY_REQUEST);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(this, "error in  scanning", Toast.LENGTH_SHORT).show();
            return;
        }

        if (requestCode == BARCODE_READER_ACTIVITY_REQUEST && data != null) {
            Barcode barcode = data.getParcelableExtra(BarcodeReaderActivity.KEY_CAPTURED_BARCODE);
            Log.d(TAG, barcode.rawValue);
            Toast.makeText(this, barcode.rawValue, Toast.LENGTH_SHORT).show();
            locationId = barcode.rawValue;
        }

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    class ImageRecognizer extends AsyncTask<Mat, Void, Response> {

        private void imageKeypointPrepare(Mat img) throws IOException {
            img1 = img.clone();
            Imgproc.cvtColor(img1, img1, Imgproc.COLOR_RGB2GRAY);
            img1.convertTo(img1, CV_8U); //converting the image to match with the type of the cameras image
            descriptors1 = new Mat();
            MatOfKeyPoint keypoints1 = new MatOfKeyPoint();
            detector.detect(img1, keypoints1);
            descriptor.compute(img1, keypoints1, descriptors1);

        }

        private double calculateDifference(Mat img) {
            Mat img2 = img.clone();
            Imgproc.cvtColor(img2, img2, Imgproc.COLOR_RGB2GRAY);
            img2.convertTo(img2, CV_8U);
            Mat descriptors2 = new Mat();
            MatOfKeyPoint keypoints2 = new MatOfKeyPoint();
            detector.detect(img2, keypoints2);
            descriptor.compute(img2, keypoints2, descriptors2);
            try {
                // Matching
                MatOfDMatch matches = new MatOfDMatch();
                if (img1.type() == img2.type()) {
                    matcher.match(descriptors1, descriptors2, matches);
                } else {
                    return 100.0;
                }
                List<DMatch> matchesList = matches.toList();

                double min_dist = 100.0;

                for (int i = 0; i < matchesList.size(); i++) {
                    Double dist = (double) matchesList.get(i).distance;
                    if (dist < min_dist)
                        min_dist = dist;
                }

                return min_dist;
            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
                return 100.0;
            }
        }

        private Response doFileUpload(Bitmap bitmap){
            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            DataInputStream inStream = null;

            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary =  "*****";

            String urlString = "http://192.168.0.152:8000/recognize/";

            try
            {
                URL url = new URL(urlString);

                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);

                dos = new DataOutputStream( conn.getOutputStream() );

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"image.jpg\"" + lineEnd);
                dos.writeBytes(lineEnd);

                Log.e("MediaPlayer","Headers are written");

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                dos.write(byteArray, 0, byteArray.length);
                dos.writeBytes(lineEnd);

                dos.writeBytes(twoHyphens + boundary + lineEnd);

                dos.writeBytes("Content-Disposition: form-data; name=\"hash\"" + lineEnd);
                dos.writeBytes(lineEnd);
                dos.writeBytes(locationId);
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                conn.getInputStream()));
                String inputLine;
                Response ronaldo = null;
                while ((inputLine = in.readLine()) != null) {
                    ronaldo = new ObjectMapper().readValue(inputLine, Response.class);
                    Log.e("[Test]", inputLine);
                }
                Log.e("MediaPlayer","File is written");
                dos.flush();
                dos.close();

                return ronaldo;
            }
            catch (MalformedURLException ex)
            {
                Log.e("MediaPlayer", "error: " + ex.getMessage(), ex);
            }
            catch (IOException ioe)
            {
                Log.e("MediaPlayer", "error: " + ioe.getMessage(), ioe);
            }

            try {
                inStream = new DataInputStream( conn.getInputStream() );
                String str;

                while (( str = inStream.readLine()) != null)
                {
                    Log.e("MediaPlayer","Server Response"+str);
                }

                inStream.close();
            }
            catch (IOException ioex){
                Log.e("MediaPlayer", "error: " + ioex.getMessage(), ioex);
            }

            return null;
        }

        private Mat imagePrepare(Mat img) {
            Mat resizeimage = new Mat();
            Size sz = new Size(224,224);
            Imgproc.resize( img, resizeimage, sz );
            return resizeimage;
        }

        @Override
        protected Response doInBackground(Mat... params) {
            Mat img= imagePrepare(params[0]);
            if (img1 == null) {
                try {
                    imageKeypointPrepare(img);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            double dist = calculateDifference(img);

            if (dist > 25.0) {
                try {
                    imageKeypointPrepare(img);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Core.rotate(img, img, Core.ROTATE_90_CLOCKWISE);
                Bitmap bitmap = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.RGB_565);
                Utils.matToBitmap(img, bitmap);
                return doFileUpload(bitmap);
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Response result) {
            super.onPostExecute(result);

            if (result != null) {
                if (result.artwork != null) {
                    Log.d(TAG, "Work!!!!");
                    Log.d(TAG, result.artwork.title);
                    tvName.setText(result.artwork.title);
                    tvDescription.setText(result.artwork.description);
                    tvAuthor.setText(result.artwork.author);
                    tvGenre.setText(result.artwork.genre);
                    tvYear.setText(String.valueOf(result.artwork.year));
//                    bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    bottomSheetBehavior.setPeekHeight((int)(80 * scale + 0.5f));
                    llBottomSheet.setZ(2000);
//                    bottomSheetBehavior.
//                    mLayout.setPanelHeight(64);
//                    drawPanel.setBackgroundColor(Color.GREEN);
//                    drawPanel.setZ(2);
//                    mLayout.setAnchorPoint(0);
//                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.ANCHORED);
//                    drawPanel.setVisibility(View.VISIBLE);
//                    mLayout.setPanelHeight(200);
//                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
//                    mLayout.setTouchEnabled(true);
//                    mLayout.setPanelHeight(200);
//                    mLayout.requestLayout();
//                    if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.HIDDEN) {
//                        mLayout.setAnchorPoint(0.8f);
//                        drawPanel.setTop(200);
//                        drawPanel.setVisibility(View.VISIBLE);
//                        mLayout.setPanelHeight(200);
//                        Log.d(TAG, String.valueOf(mLayout.getPanelHeight()));
//                        Log.d(TAG, mLayout.getPanelState().name());
                    //                    mLayout.invalidate();
//                        mLayout.setOverlayed(true);
//                        drawPanel.setVisibility(View.VISIBLE);
//                        mLayout.requestLayout();
//                    }

//                    mLayout.
//                    mLayout.setOverlayed(true);
                } else {
                    Log.d(TAG, tvName.toString());
                    Log.d(TAG, "Work");
                    tvName.setText("");
                    tvDescription.setText("");
                    tvAuthor.setText("");
                    tvGenre.setText("");
                    tvYear.setText("");
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//                    drawPanel.setVisibility(View.INVISIBLE);
//                    mLayout.setPanelHeight(0);
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(PortraitCameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        Mat b = inputFrame.rgba();

        if ((task == null || task.getStatus() == AsyncTask.Status.FINISHED || task.getStatus() == AsyncTask.Status.PENDING) && enableRequest) {
            task = new ImageRecognizer();
            task.execute(b);
        }

        return b;

    }
}
